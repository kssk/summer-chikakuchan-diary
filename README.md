# サマーちかくちゃんダイアリー一式

[サマーちかくちゃんダイアリー](https://eidantoei.org/HR2kdhrM/) の日記データと日記システム一式

## 日記を書いてから公開するまで

0. 日記をかく

    ```bash
    hugo new post/ビューティフルなタイトル.md
    atom content/post/ビューティフルなタイトル.md
    ```
0. 静的ファイルを生成

    ```bash
    hugo
    ```
0. コミット・プッシュ

    ```bash
    git add content/* public/* && git commit -m '日記書いた' && git push
    ```
0. GitLab CI が勝手に Amazon S3 にアップロードしてくれる
 - see ☞ `.gitlab-ci.yml`

## メモ

- あらかじめやること: GitLab プロジェクトに Variables をセット
 - S3にアップロードするためのCredential
 - `AWS_ACCESS_KEY_ID`
 - `AWS_SECRET_ACCESS_KEY`
